package com.example.dxe.slideshow;

import java.io.Serializable;

/**
 * Created by dxe on 4/28/15.
 */
public class MediaItem implements Serializable {
    private static final long serialVersionUID = 1L; // class's version number

    // constants for media types
    public static enum MediaType {IMAGE, VIDEO};

    private MediaType type; // this item is IMAGE or VIDEO
    private String path; // location of this MediaItem

    public MediaItem(MediaType mediaType, String location) {
        type = mediaType;
        path = location;
    }

    public MediaType getType() {
        return type;
    }

    public String getPath() {
        return path;
    }
}
